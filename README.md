### Usage

```bash
helm install \
  haproxy-ingress \
  haproxytech/kubernetes-ingress \
  -n internal \
  -f haproxy-ingress-values.yaml \
  --version 1.17.11
```

