ARG DATAPLANE_MINOR=${DATAPLANE_MINOR}
ARG INGRESS_VERSION=${INGRESS_VERSION}

FROM golang:latest AS builder

ARG DATAPLANE_MINOR
ENV DATAPLANE_MINOR $DATAPLANE_MINOR
ENV DATAPLANE_URL https://github.com/haproxytech/dataplaneapi.git

RUN set -x \
    && git clone "${DATAPLANE_URL}" "${GOPATH}/src/github.com/haproxytech/dataplaneapi" \
    && cd "${GOPATH}/src/github.com/haproxytech/dataplaneapi" \
    && git checkout "v${DATAPLANE_MINOR}" \
    && make build \
    && cp build/dataplaneapi /dataplaneapi


FROM haproxytech/kubernetes-ingress:${INGRESS_VERSION}

COPY --from=builder /dataplaneapi /usr/local/bin/dataplaneapi

RUN set -x \
    && sed -i "s| -f /etc/haproxy/haproxy-aux.cfg||g" /etc/services.d/haproxy/run \
    && chmod +x /usr/local/bin/dataplaneapi \
    && ln -s /usr/local/bin/dataplaneapi /usr/bin/dataplaneapi \
    && touch /usr/local/etc/haproxy/dataplaneapi.hcl \
    && chown "$HAPROXY_UID:$HAPROXY_GID" /usr/local/etc/haproxy/dataplaneapi.hcl
